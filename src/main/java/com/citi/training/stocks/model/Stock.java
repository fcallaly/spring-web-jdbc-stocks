package com.citi.training.stocks.model;

public class Stock {

    private long id;
    private String ticker;
    private String companyName;

    public Stock() {}

    public Stock(long id, String ticker, String companyName) {
        this.id = id;
        this.ticker = ticker;
        this.companyName = companyName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return "Stock [id=" + id + ", ticker=" + ticker + ", companyName=" + companyName + "]";
    }

    public boolean equals(Stock stock) {
        return this.id == stock.getId() &&
               this.ticker.equals(stock.getTicker()) &&
               this.companyName.equals(stock.getCompanyName());
    }
}

package com.citi.training.stocks.dao;

import java.util.List;

import com.citi.training.stocks.model.Stock;

public interface StockDao {
    List<Stock> findAll();

    Stock findById(long id);

    void deleteById(long id);

    Stock save(Stock stock);
}

package com.citi.training.stocks.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.stocks.model.Stock;

@Component
public class MysqlStockDao implements StockDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Stock> findAll() {
	// to be completed with jdbcTemplate.query and StockMapper
        return null;
    }

    public Stock findById(long id) {
	// to be completed with jdbcTemplate.query and StockMapper
        return null;
    }

    public void deleteById(long id) {
	// to be completed with jdbcTemplate.update
    }

    public Stock save(Stock stock) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        this.jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                            "insert into stock (ticker, company_name) values (?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, stock.getTicker());
                ps.setString(2,  stock.getCompanyName());
                return ps;
            }
        },
        keyHolder);

        stock.setId(keyHolder.getKey().longValue());
        return stock;
    }

    private static final class StockMapper implements RowMapper<Stock> {
        public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
            Stock stock = new Stock(rs.getInt("id"),
                                    rs.getString("ticker"),
                                    rs.getString("company_name"));
            return stock;
        }
    }

}
